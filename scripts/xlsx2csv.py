#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import re
import numpy as np
import pandas as pd
import openpyxl as xlsx
from openpyxl.utils.cell import get_column_interval, rows_from_range, cols_from_range

print(__file__)
PRJDIR = os.path.abspath(os.path.join(os.path.split(__file__)[0], '..'))
print(PRJDIR)
DATDIR = os.path.join(PRJDIR, 'data')
print(DATDIR)

XLSXFILE = os.path.join(DATDIR, 'building_stock.xlsx')
# define headers: from, to
# 2: topics: buildings, walls, ...
# 3: features: Area, Tenure, Ownership, ...
# 4: types: Covered area: constructed [Mm²], heated [Mm²], cooled [Mm²], ..
# 5: details: insulation, low-e


TOPIC = ("F2", "CP2")  # ("F3", "T3"), ("CG3", "CP3")
KTOPIC = {"BUILDING": ('F2', 'T2'),
          "ENERGY": ('CG2', 'CP2'), }
FEATURES = ("F3", "CP3")  # ("F3", "T3"), ("CG3", "CP3")
TYPES = ("F4", "CP4")
DETAILS = ("F5", "CP5")
CATEGORY = ("F3", "CP3")

# define indexes: from, to
SECTOR = "B6", "B94"
SUBSEC = "C6", "C94"
BUILDING_TYPE = "D6", "D94"
BUILDING_AGE = "E6", "E94"

#       cols         rows
DATA = ("F", "CP"), (6, 94)

UNITS = {'Mm²': 'Mm²',
         '': 'dimensionless',
         'Mil.': '1e6 dimensionless',
         'kWh/m² year': 'kWh/m²/year',
         'TWh/year': 'TWh/year',
         'MIl.': '1e6 dimensionless'}

COUNTRIES = {'Austria': 'at',
             'Belgium': 'be',
             'Bulgaria': 'bg',
             'Croatia': 'hr',
             'Cyprus': 'cy',
             'Czech Republic': 'cz',
             'Denmark': 'dk',
             'Estonia': 'ee',
             'Finland': 'fi',
             'France': 'fr',
             'Germany': 'de',
             'Greece': 'gr',
             'Hungary': 'hu',
             'Ireland': 'ie',
             'Italy': 'it',
             'Latvia': 'lv',
             'Lithuania': 'lt',
             'Luxembourg': 'lu',
             'Malta': 'mt',
             'Netherlands': 'nl',
             'Poland': 'pl',
             'Portugal': 'pt',
             'Romania': 'ro',
             'Slovakia': 'sk',
             'Slovenia': 'si',
             'Spain': 'es',
             'Sweden': 'se',
             'United Kingdom': 'uk',
             'EU27+UK': 'eu27+UK'}

# Cell mapping for types
TYPE_MAPPING = {
    'Covered area: constructed [Mm²]': 'Constructed area [Mm²]',
    'Covered area: cooled [Mm²]': 'Cooled area [Mm²]',
    'Covered area: heated [Mm²]': 'Heated area [Mm²]'
}

OUTPUT_COLUMNS = ["country",
                  "country_code",
                  "sector",
                  "subsector",
                  "btype",
                  "bage",
                  "topic",
                  "feature",
                  "type",
                  "detail",
                  "estimated",
                  "comment",
                  "value",
                  "unit",
                  "source"]

# Sources by column
SOURCES_COLUMNS = {
    "F": "Vienna University of Technology, e-think. Invert/EE-Lab. 2017 http://www.invert.at/",
    "G": "S. Pezzutto. Analysis of the space heating and cooling market in Europe. PhD-Thesis. 2014",
    ("I", "P"): "Own calculations",
    ("Q",
     "T"): "Source residential part: IEE TABULA Project, TABULA Web Tool, 2017 http://webtool.building-typology.eu/#bm; Source service part: S. Birchall et al., D2.1a Survey on the energy needs and architectural features of the EU building stock, 2014",
    ("U", "CF"): "Source residential part: Own calculations",
    ("CG", "CL"): "Vienna University of Technology, e-think. Invert/EE-Lab. 2017 http://www.invert.at/",
    ("CM",
     "CP"): "Vienna University of Technology, e-think. Invert/EE-Lab. 2017 http://www.invert.at/ and S. Pezzutto. Analysis of the space heating and cooling market in Europe. PhD-Thesis. 2014"
}

# Sources by specific cell range
SOURCES_CELLS = {
    "Austria": {
        "R49": "M. Eder. Nachhaltige Bürogebäude. 2009",
        "CG23": "S.Pezzutto. Analysis of the space heatingand cooling market in Europe. PhD Thesis. 2014",
        ("CL23",
         "CL29"): "S. Pezzutto, A. Toleikytė, M. De Felice. Assessment of the Space Heating and Cooling Market in the EU28: A Comparison between EU15 and EU13 Member States. http://contemporary-energy.net/Articles/v01n02a05_-Simon-Pezzutto-et-al.pdf",
        ("CL31",
         "CL37"): "S. Pezzutto, A. Toleikytė, M. De Felice. Assessment of the Space Heating and Cooling Market in the EU28: A Comparison between EU15 and EU13 Member States. http://contemporary-energy.net/Articles/v01n02a05_-Simon-Pezzutto-et-al.pdf"
    },
    "Bulgaria": {
        ("F47", "F53"): "BPIE. Data Hub for the Energy Performance of Buildings. 2015 https://www.buildingsdata.eu"
    },
    "Czech Republic": {
        "F15": "T. Csoknyai et al. Building stock characteristics and energy performance of residentialbuildings in Eastern-European countries. 2016"
    },
    "Italy": {
        ("K7", "K13"): "P. Melograno, S. Pezzutto. Italian country report. IEA. 2010",
        ("K15", "K21"): "P. Melograno, S. Pezzutto. Italian country report. IEA. 2010",
        ("K23", "K29"): "P. Melograno, S. Pezzutto. Italian country report. IEA. 2010",
        ("K31", "K37"): "P. Melograno, S. Pezzutto. Italian country report. IEA. 2010",
        "CG15": "H. Mahlknecht, S. Avesani, M. Benedikter, and G. Felderer, Refurbishment and monitoring of an historical building. A case Study. 2009",
        "CH48": "S. Pezzutto, F. Haas, D. Exner. Europe´s Building Stock and its energy demand: a Comparison between Austria and Italy. 2017",
    },
    "Romania": {
        "F55": "RePublic¬_ZEB. D2.1 Report on the preliminary assement of public building stock. 2014 http://www.republiczeb.org/filelibrary/WP2/D2-1Public-Building-Stock-final.pdf"
    },
    "Slovenia": {
        "F15": "Episcope. Episcope Data Search. 2017 https://www.buildingsdata.eu/episcope-data/results"
    },
    "Sweden": {
        ("CA15", "CA21"): "EHPA. European Heat pump News. 2007 http://www.ehpa.org/uploads/media/EHPA_Newslett",
        ("CA23", "CA29"): "EHPA. European Heat pump News. 2007 http://www.ehpa.org/uploads/media/EHPA_Newslett",
        ("CA31", "CA37"): "EHPA. European Heat pump News. 2007 http://www.ehpa.org/uploads/media/EHPA_Newslett",
    },
}


# ----------------
# Source retrieval
# ----------------
class SourceMapping:
    """
    Class to map source column to text
    """

    def __init__(self, source_list: dict):
        """
        Class constructor
        :param source_list: List of sources (source column -> source text)
        """
        column_map = []
        for column_range, source_text in source_list.items():
            if type(column_range) is tuple:
                start, end = column_range
                columns = set(get_column_interval(start=start, end=end))
            else:
                columns = {column_range}

            column_map.append((columns, source_text))

        self.column_map = column_map

    def source_by_column(self, column: str) -> str:
        """
        Given the column position, retrieve source for value
        :param column: Column position
        :return: Source text
        """
        for column_range, source_text in self.column_map:
            if column in column_range:
                return source_text
        return ''


def get_column_range(cells) -> set:
    """
    Given a cell / cell range, return all cells inside range
    :param cells: Cell / range of cells
    :return: List of cells
    """
    if type(cells) is tuple:
        start_cell, end_cell = cells

        # Construct range
        range_str = '{start}:{end}'.format(start=start_cell, end=end_cell)

        # List of cells belonging to such range
        range_tuples = set(
            [item for row in cols_from_range(range_str) for item in row] + [item for row in
                                                                            rows_from_range(range_str) for item
                                                                            in row])
    else:
        range_tuples = {cells}

    return range_tuples


def map_individual_source(source_columns, cell):
    """
    Given a list of sources, try to retrieve source
    :param source_columns: List of sources by cell range
    :param cell: Cell to check
    :return: Source for specific cell, None if not found
    """
    source = None
    for column_range, source_text in source_columns.items():
        # List of cells in range
        range_cells = get_column_range(cells=column_range)

        # Check cell inside range
        if cell in range_cells:
            source = source_text
            break

    return source


def map_source(page_name, cell, sources):
    """
    Return source text for a cell from a list of pages
    :param page_name: Name of page for document
    :param cell: Cell to check source
    :param sources: List of sources, divided by page name -> cell
    :return: Source text
    """
    # Default source
    source = None

    try:
        source_columns = sources[page_name]

        # Find specific source for cell
        source_tmp = map_individual_source(source_columns=source_columns, cell=cell)

        if source_tmp is not None:
            source = source_tmp

    except KeyError:
        pass

    return source


# Object to map source column to text
source_mapping = SourceMapping(source_list=SOURCES_COLUMNS)


def get_source_text(page_name, cell, column):
    """
    Get source text by cell/column
    :param page_name: Page name
    :param cell: Cell to check
    :param column: Column to check if no source found in cell
    :return: Source value
    """
    if page_name == 'EU28':
        source = 'Own calculations'
    else:
        # Try to get source by cell
        source = map_source(page_name=page_name, cell=cell, sources=SOURCES_CELLS)

        if source is None:
            # Try to get source by column
            source = source_mapping.source_by_column(column=column)

    return source


def extract_cells(sheet, indexes):
    if not isinstance(indexes[0], tuple):
        indexes = [indexes, ]
    res = []
    for start, end in indexes:
        res.extend(sheet[start:end][0])
    return res


def remove_line_change(text):
    """
    Remove change of line for a text
    :param text: Text to check
    :return: Text with no change of line
    """
    try:
        return text.replace('\n', ' ').replace('\r', '')
    except AttributeError:
        return text


def read_sheet_header_row(sheet, cells, fillmissing=False, feature_mapping=None,
                          get_value=lambda cell: cell.value, **labels):
    """Read header from excel."""
    labels = {cell: label
              for label, (start, stop) in labels.items()
              for cells in sheet[start:stop] for cell in cells}
    old_lab = None
    for cell in cells:
        lab = labels.get(cell, None)
        if lab is None:
            lab = get_value(cell)
            if fillmissing:
                if lab is None:
                    lab = old_lab
                else:
                    old_lab = lab

        # Remove change of line from text
        lab = remove_line_change(text=lab)

        # Map cell text if necessary
        if feature_mapping is not None and lab in feature_mapping:
            lab = feature_mapping[lab]

        if np.isreal(lab):
            lab = None
        yield lab


def read_sheet_header2(sheet, itopics, ifeatures, itypes, idetails, ktopics):
    """Read the sheet header"""
    return zip(read_sheet_header_row(sheet, extract_cells(sheet, itopics),
                                     fillmissing=True, **ktopics),
               read_sheet_header_row(sheet, extract_cells(sheet, ifeatures),
                                     fillmissing=True),
               read_sheet_header_row(sheet, extract_cells(sheet, itypes),
                                     feature_mapping=TYPE_MAPPING, fillmissing=False),
               read_sheet_header_row(sheet, extract_cells(sheet, idetails),
                                     fillmissing=False))


def read_sheet_header(sheet, itopics, icats):
    """Read the sheet header"""
    otopic = None
    for topic, category in zip(extract_cells(sheet, itopics),
                               extract_cells(sheet, icats)):
        top, cat = topic.value, category.value
        if top:
            otopic = top
        else:
            top = otopic
        yield top, cat


def read_sheet_index(sheet, isector, isubsec, ibtype, ibage):
    """Read the sheet index"""
    for sector, subsector, btype, bage in zip(sheet[isector[0]:isector[1]],
                                              sheet[isubsec[0]:isubsec[1]],
                                              sheet[ibtype[0]:ibtype[1]],
                                              sheet[ibage[0]:ibage[1]]):
        try:
            yield (sector[0].value, subsector[0].value,
                   btype[0].value, bage[0].value)
        except BaseException as exc:
            print(sector, subsector, btype, bage)


def read_sheet(wkb,
               sheet_name,
               itopics,
               ifeatures,
               itypes,
               idetails,
               ktopics,
               isector,
               isubsec,
               ibtype,
               ibage,
               idata,
               country,
               country_code):
    """Read the sheet"""
    sheet = wkb[sheet_name]
    columns = list(read_sheet_header2(sheet, itopics, ifeatures,
                                      itypes, idetails, ktopics))

    indexes = list(read_sheet_index(sheet, isector, isubsec, ibtype, ibage))
    icols, irows = idata
    data = []

    for row, index in zip(range(irows[0], irows[1]), indexes):
        for cell, cols in zip(sheet[icols[0] + str(row): icols[1] + str(row)][0],
                              columns):
            comment, value = cell.comment, cell.value
            estimated_value = 1 if cell.fill.fgColor.rgb == 'FF7F7F7F' or cell.fill.fgColor.tint == -0.499984740745262 else 0

            if comment or value:
                data.append(
                    [
                        country,  # Country
                        country_code  # Country code
                    ] +
                    list(index) +
                    list(cols) +
                    [
                        estimated_value,  # Value
                        '' if comment is None else comment.text, value,  # Comment
                        get_source_text(page_name=country, cell=cell.coordinate, column=cell.column)  # Source
                    ]
                )

    return data


def read_workbook(workbook_name,
                  itopics, ifeatures, itypes, idetails, ktopics,
                  isector, isubsec, ibtype, ibage,
                  idata):
    """Read the whole document sheet by sheet"""
    wkb = xlsx.load_workbook(workbook_name, read_only=False, data_only=True)
    data = []
    for sheet_name in wkb.sheetnames:
        dt = read_sheet(wkb=wkb,
                        sheet_name=sheet_name,
                        itopics=itopics,
                        ifeatures=ifeatures,
                        itypes=itypes,
                        idetails=idetails,
                        ktopics=ktopics,
                        isector=isector,
                        isubsec=isubsec,
                        ibtype=ibtype,
                        ibage=ibage,
                        idata=idata,
                        country=sheet_name,
                        country_code=COUNTRIES[sheet_name])
        print(sheet_name, len(dt))
        data.extend(dt)
    print(len(data))

    columns = OUTPUT_COLUMNS[:]
    columns.remove("unit")

    return pd.DataFrame(data, columns=columns)


def extract_units(df, column='type', regexp=".*\[(.+)\].*"):
    rgxp = re.compile(regexp)
    for i, txt in df[column].items():
        try:
            match = rgxp.match(txt)
        except TypeError:
            print("fail:", txt)
        df.at[i, 'unit'] = UNITS['' if match is None else match.groups()[0]]


def isnumber(value):
    try:
        float(value)
    except:
        return False
    return True


if __name__ == "__main__":
    # wkb = xlsx.load_workbook(XLSXFILE, read_only=False, data_only=True)
    # sheet_name = "Austria"
    # dt = read_sheet(wkb, sheet_name,
    #                 TOPIC, FEATURES, TYPES, DETAILS, KTOPIC,
    #                 SECTOR, SUBSEC, BUILDING_TYPE, BUILDING_AGE,
    #                 DATA, country=sheet_name,
    #                 country_code=COUNTRIES[sheet_name])
    # df = pd.DataFrame(dt, columns=["country", "country_code",
    #                                "sector", "subsector", "btype", "bage",
    #                                "topic", "feature", "type", "detail",
    #                                "comment", "value"])
    # -------------------------------------------------------------------
    building_stock = read_workbook(XLSXFILE,
                                   TOPIC, FEATURES, TYPES, DETAILS, KTOPIC,
                                   SECTOR, SUBSEC, BUILDING_TYPE, BUILDING_AGE,
                                   DATA)

    # extract units
    extract_units(building_stock, column='type', regexp=".*\[(.+)\].*")

    # drop comments
    building_stock.pop("comment")

    # change order to columns as desired
    ordered_columns = OUTPUT_COLUMNS[:]
    ordered_columns.remove("comment")
    building_stock = building_stock[ordered_columns]

    # extract where value is a number
    numb = building_stock.value.apply(isnumber)
    numeric_building_stock = building_stock[(numb == True)]
    not_numeric_building_stock = building_stock[(numb == False)]

    # write to CSV
    numeric_building_stock.to_csv(os.path.join(DATDIR, "building_stock.csv"),
                                  sep="|", index=False)
    not_numeric_building_stock.to_csv(os.path.join(DATDIR, "DHW_spaceHC.csv"),
                                      sep="|", index=False)
    # read from CSV
    # test = pd.read_csv(os.path.join(DATDIR, "building_stock.csv"), sep="|")
